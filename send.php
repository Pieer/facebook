<?php
	require 'config.php';

	$name       = (isset($_POST['name'])) ? strip_tags($_POST['name']) : '';
	$email      = (isset($_POST['email'])) ? strip_tags($_POST['email']) : '';
	$age        = (isset($_POST['age']))   ? strip_tags($_POST['age']) : '';
	$state      = (isset($_POST['state'])) ? strip_tags($_POST['state']) : '';
	$tag        = (isset($_POST['tag']))  ? strip_tags($_POST['tag']) : '';
	$facebook   = (isset($_POST['facebook']))   ? strip_tags($_POST['facebook']) :   '';
	$twitter    = (isset($_POST['twitter']))    ? strip_tags($_POST['twitter'])  :   '';
	$website    = (isset($_POST['website']))    ? strip_tags($_POST['website'])  :   '';
	$answer     = (isset($_POST['answer']))     ? strip_tags($_POST['answer'])   :   '';
	$passport   = (isset($_POST['passport']) && $_POST['passport'] == 'Yes')   ? strip_tags($_POST['passport']) : 'No';
	$terms      = (isset($_POST['terms'])    && $_POST['terms'] == 'Yes')      ? strip_tags($_POST['terms'])    : 'No';

	if(!$name || !$email || !$age || !$state || !$tag || !$facebook || !$twitter || !$answer || !$passport || !$terms){
	  $errorMsg = 'Please complete all mandatory fields!';
	  header('Location: index.php?errorMsg=' . $errorMsg);
	}


	if(!isset($errorMsg)){

	  $data = array('name'      =>  $name,
	                'email'     =>  $email,
					'age'       =>  $age,
					'state'     =>  $state,
					'tag'       =>  $tag,
					'facebook'  =>  $facebook,
					'twitter'   =>  $twitter,
					'website'   =>  $website,
					'answer'    =>  $answer,
					'passport'  => $passport,
					'terms'     => $terms);
					 
		// Send it in the $_POST
		try {
		  $response = submit($data);
		} catch(Exception $e) {
		  $errorMsg = $e->getMessage();
		}
		
			
		if(!isset($errorMsg)) {
		  echo 1;  	  
		} else {
		  echo $errorMsg;  
		}
		
	}

	function submit($data){
	  //URL From Smail for Form submission to the halo-fest database in the Xbox Folder
	  $url = $smail;
	  $ch = curl_init($url);
	  curl_setopt($ch, CURLOPT_POST, true);
	  curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	  $response = curl_exec($ch);  
	  curl_close($ch);	
	  
	  return $response;
	}
	
?>