<?
require 'config.php';
require 'facebook.php';
require 'mdetect.php';


$facebook = new Facebook(array(
'appId' => $app_id,
'secret' => $app_secret,
'cookie' => true
));

$signed_request = $facebook->getSignedRequest();

$page_id = $signed_request["page"]["id"];
$page_admin = $signed_request["page"]["admin"];
$like_status = $signed_request["page"]["liked"];
$country = $signed_request["user"]["country"];
$locale = $signed_request["user"]["locale"];

// Checks for mobile/iFrame functionality.
$uagent = new uagent_info();
$IsMobile = $uagent->DetectTierIphone() || $uagent->DetectTierTablet();

$IniFrame = isset($_REQUEST['signed_request']);

if($live){
	if (! ($IniFrame || $IsMobile) ) {
	        header('location:<? echo $location ?>');
	        exit();
	}
}
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title><? echo $title ?></title>
  <link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/2.9.0/build/reset/reset-min.css" />
  <link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/2.9.0/build/fonts/fonts-min.css" />  
  <link href="css/style.css" rel="stylesheet" type="text/css" />
  <link href="js/fancybox/jquery.fancybox-1.3.4.css" rel="stylesheet" type="text/css" />  
  <script type="text/javascript" src="js/jquery-1.5.2.min.js"></script>  
  <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>    
  <script type="text/javascript" src="js/popup.js"></script>      
  <script type="text/javascript" src="js/form.js"></script>
  
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', '<? echo $GoogleAnalytic?>']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>

</head>
<body>
<div id="fb-root"></div>
<script src="http://connect.facebook.net/en_US/all.js"></script>
<script> 
    FB.init({   
        appId  : '<? echo $app_id ?>',   
        status : false, // check login status   
        cookie : true, // enable cookies to allow the server to access the session   
        xfbml  : true  // parse XFBML 
    });   
    window.fbAsyncInit = function() {
        FB.Canvas.setSize({ width: 520, height: 2250 }); // Live in the past    
    }
</script>