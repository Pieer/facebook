<form class="test" method="post" action="send.php">		
	<div class="message success">
		<strong><? echo $successMessage ?><br></strong>
	</div>
				
	<div class="sending">
	 <strong>Entering the competition...</strong>
	</div>
	
	<? if(isset($_GET['errorMsg'])) { ?>
	  <div class="red">
		<?=$_GET['errorMsg'] ?>
	  </div>	
	  <br />
	<? } ?> 
	

	<!-- 
		!!!!!	Select the design of your form by removing or adding those class: nolabel inlinelabel toplabel 
	-->


	<div class="form-fields nolabel">				
		<div class="form-row">
		  <label for="name">Name:</label>
		  <div class="field"><input id="name" type="text" value="" name="name" class="clear"/></div>
		</div>
		
		<div class="form-row">
			<label for="email">Email:</label>
		  <div class="field"><input id="email" type="text" value="" name="email" class="clear"/></div>
		</div>
		
		<div class="form-row">
			<label for="age">Age:</label>
			<div class="styled-select">
				<select name="age">
					<option value="Age">Age</option>
					<option value="13-17">13-17</option>
					<option value="18-25">18-25</option>
					<option value="26-30">26-30</option>
					<option value="31-45">31-45</option>
					<option value="Over 45">Over 45</option>
				</select>
			</div>	
		</div>
		<div class="form-row">
			<label for="state">State:</label>
			<div class="styled-select">
				<select name="state">
					<option value="State">State</option>
					<option value="NSW">NSW</option>
					<option value="VIC">VIC</option>
					<option value="VIC">QLD</option>
					<option value="VIC">TAS</option>
					<option value="VIC">WA</option>
					<option value="VIC">ACT</option>
					<option value="VIC">SA</option>
				</select>
			</div>								  
		</div>
		
		<div class="form-row">
		  <label for="tag">Tag:</label>
		  <div class="field"><input id="tag" type="text" value="" name="tag" class="clear"/></div>
		</div>
										
		<div class="form-row">
	      <label for="facebook">Facebook:</label>
		  <div class="field"><input id="fb" type="text" value="" name="facebook" class="clear"/></div>
		</div>
													
		<div class="form-row">
		  <label for="twitter">Twitter:</label>
		  <div class="field"><input id="tw" type="text" value="" name="twitter" class="clear"/></div>
		</div>
		
		<div class="form-row">
		  <label for="website">Website:</label>
		  <div class="field"><input id="website" type="text" value="" name="website" class="clear"/></div>
		</div>
										
		<div class="form-row">
		  <label for="answer">Answer:</label>
		  <div class="field"><textarea id="answer" name="answer" rows="5" maxlength="360" class="clear"></textarea></div>
		</div>
										
		<div class="form-row">
		  <div class="field"><input type="checkbox" name="passport" value="Yes" />
			  <div class="checks">
			  	<span class="check">I am available on these dates </span><span class="check"><br>and I can make my own way to the event.</span> 
			  </div>
			</div>									
		</div>
		
		<div class="form-row">
		  <div class="field"><input type="checkbox" name="terms" value="Yes" />
		  		<!-- keep id = terms if you want to link it to the lightbox -->
				<span class="check">I agree to the <a href="#lightbox" id="terms">Terms and Conditions.</a></span> 
			</div>
		</div>
										
		<div class="form-row">
		  <div class="field"><input class="button" type="image" src="img/enter-now.png" alt="Submit Form" /></div>
		</div>
															
	</div>
</form>
