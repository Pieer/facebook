jQuery(function() {
  var age, answer, email, facebook, isEmpty, isNumeric, name, state, tag, twitter, validEmail, website;
  isEmpty = function(value) {
    if (!jQuery.trim(value)) {
      return true;
    } else {
      return false;
    }
  };
  validEmail = function(email) {
    var regmail = regmail = /^[a-z0-9][^\(\)\<\>\@\,\;\:\\\"\[\]]*\@[a-z0-9\.\-]*\.[a-z]{2,4}$/i;
    if (isEmpty(email) || !email.match(regmail)) {
      return false;
    } else {
      return true;
    }
  };
  isNumeric = function(val) {
    if ((val - 0) === val && val > 0) {
      return true;
    } else {
      return false;
    }
  };
  name = "Name";
  email = "E-Mail";
  tag = "Gamer Tag";
  facebook = "Facebook";
  twitter = "Twitter";
  website = "Website";
  answer = "persuade us in 360 CHARACTERS OR LESS";
  age = "Age";
  state = "State";
  $("#name").val(name);
  $("#email").val(email);
  $("#tag").val(tag);
  $("#fb").val(facebook);
  $("#tw").val(twitter);
  $("#answer").val(answer);
  $("#website").val(website);
  $("#age").val(age);
  $("#state").val(state);
  $("input.clear, textarea.clear").each(function() {
    $(this).data("default", $(this).val()).addClass("inactive").focus(function() {
      $(this).removeClass("inactive");
      if ($(this).val() === $(this).data("default") || "") {
        $(this).val("");
      }
    }).blur(function() {
      var default_val;
      default_val = $(this).data("default");
      if ($(this).val() === "") {
        $(this).addClass("inactive");
        $(this).val($(this).data("default"));
      }
    });
  });

  $("form").submit(function(e) {
    var allInputs, data, message;
    $("div.field input").removeClass("error");
    $("div.field span").hide();
    $("div.field span.check").show();
    allInputs = $("form :input");
    message = "";
    data = "";
    jQuery.each(allInputs, function() {
      switch (this.name) {
        case "name":
          if (isEmpty(this.value) || this.value === name) {
            $(this).addClass("error").after("<span class=\" red err-msg\">Please enter your Name.</span>");
            message = "There is an issue with the data provided, please refer to information in red.";
          }
          break;
        case "email":
          if (!validEmail(this.value) || this.value === email) {
            $(this).addClass("error").after("<span class=\" red err-msg\">Please enter a valid Email.</span>");
            message = "There is an issue with the data provided, please refer to information in red.";
          }
          break;
        case "age":
          if (this.value === age) {
            $(this).parent().addClass("error").after("<span class=\" red err-msg\">Please select your age.</span>");
            message = "There is an issue with the data provided, please refer to information in red.";
          }
          break;
        case "state":
          if (this.value === state) {
            $(this).parent().addClass("error").after("<span class=\" red err-msg\">Please select your state.</span>");
            message = "There is an issue with the data provided, please refer to information in red.";
          }
          break;
        case "tag":
          if (isEmpty(this.value) || this.value === tag) {
            $(this).addClass("error").after("<span class=\" red err-msg\">Please enter Gamer Tag.</span>");
            message = "There is an issue with the data provided, please refer to information in red.";
          }
          break;
        case "answer":
          if (isEmpty(this.value) || this.value === answer) {
            $(this).addClass("error").after("<span class=\" red err-msg\">Please enter your answer.</span>");
            message = "There is an issue with the data provided, please refer to information in red.";
          }
          break;
        case "passport":
          if (!$(this).attr("checked")) {
            $(this).addClass("error").next(".checks").after("<span class=\" red err-msg\">You need to be able to make your own way to the events at the specified time.</span>");
            message = "There is an issue with the data provided, please refer to information in red.";
          } else {
            $(this).attr("value", "Yes");
          }
          break;
        case "terms":
          if (!$(this).attr("checked")) {
            $(this).addClass("error").next(".check").after("<span class=\" red err-msg\">You must agree to the Terms and to enter this competition.</span>");
            message = "There is an issue with the data provided, please refer to information in red.";
          } else {
            $(this).attr("value", "Yes");
          }
      }
      if (isEmpty(message)) {
        if (isEmpty(data)) {
          return data += this.name + "=" + this.value;
        } else {
          return data += "&" + this.name + "=" + encodeURIComponent(this.value);
        }
      }
    });
    if (!isEmpty(message)) {
      return false;
    } else {
      $(".button").hide();
      $(".form-fields").hide();
      $(".sending").fadeIn("slow");
      $.ajax({
        url: "send.php",
        type: "POST",
        data: data,
        cache: false,
        success: function(html) {
          if (html === 1) {
            $(".sending").hide();
            return $(".success").fadeIn("slow");
          }
        }
      });
      return false;
    }
  });
});