<? include_once 'includes/header.php'; ?>

<div class="container">

  	<div class="header">
    	<img src="img/<? echo $banner ?>" alt=""/>
	</div>

	<div class="content">
		<div id="wrapper">
			<? if ($competitonOver){
				echo '<div class="message success"><strong>'. $endCompMessage .'</strong></div>';
				}
				else{
					include_once 'includes/form.php';
				}
			?>
		</div>
	</div>

	<div class="footer">
		<img src="img/<? echo $footerBanner ?>"/>
	</div>

	
</div>

<? include_once 'includes/footer.php'; ?>



<? if ($termAndCondition){	include_once 'includes/term.php';}	?>
